# CodeIgniter 4 Flash Library

This library simplifies the use of flashdata in CodeIgniter 4.

## Install

Install it by running `composer require steevedroz/ci4-flash`.

## Basic usage

### Register a message

When you want a specific message to display on the next page, use the `\SteeveDroz\CiFlash\Flash::add($message, $type, $translate, $samePage)` static method by providing the following informations:

- `$message` is the message you want to display.
- `$type` is the type of message, by default, *info*. This is completely free, but examples would be *warning* or *error*.
- `$translate` will display the message when false (by default) and pass it into `lang('Flash.' . $message)` if true.
- `$samePage` will display the message on the same page instead of the next page. Default value: false.

### Display the messages

By inserting `\SteeveDroz\CiFlash\Flash::display()` in your view, a list of `<div>` will be printed, each containing a message.

### Styling the messages

Each message displayed by the method above get the CSS class `sd_flash`, plus a specific type that correspond the type.

You can simply describe how to display your flash data with the CSS:

```css
.sd_flash {
    /* for all flash messages */
}

.sd_flash.info {
    /* for the messages with "info" $type */
}

.sd_flash.whatever-you-need {
    /* for the messages with "whatever-you-need" $type  */
}
```