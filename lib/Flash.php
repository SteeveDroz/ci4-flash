<?php

namespace SteeveDroz\CiFlash;

/**
 * This class represents a flashdata message.
 *
 * In ordre to use it, pass a new \SteeveDroz\CiFlash\Flash as a flash message, to the session. It can be done in mainly two ways:
 * <ul>
 * <li><code>service('session')->setFlashdata('someUniqueKey', new \SteeveDroz\CiFlash\Flash('messageToDisplay'));</code></li>
 * <li><code>redirect()->to('some/url')->with('someUniqueKey', new \SteeveDroz\CiFlash\Flash('messageToDisplay'));</code></li>
 * </ul>
 */
class Flash
{
    /**
     *  The message to display.
     */
    protected $message;
    /**
     * The type of flash message.
     */
    protected $type;
    /**
     * Whether the message is supposed to be translated.
     */
    protected $translate;

    /**
     * List of messages that must be displayed on the same page.
     */
    protected static $samePageFlash = [];

    /**
     * Creates a new Flash.
     *
     * @param string $message   the message to be displayed or the translation key corresponding to the message
     * @param string $type      The type of message, "info" by default. This must correspond to a CSS class.
     * @param bool   $translate If false (by default), the message will be displayed as is. If true, <code>lang(Flash.$message)</code> will be called, for example: <code>new Flash('helloWorld')</code> produces the message <code>lang('Flash.helloWorld')</code>.
     */
    public function __construct(string $message, string $type = 'info', bool $translate = false)
    {
        $this->message = $message;
        $this->type = $type;
        $this->translate = $translate;
    }

    /**
     * Returns a string containing the HTML that should be displayed to show the Flash.
     *
     * @return string the HTML flash message corresponding to this Flash
     */
    public function show(): string
    {
        return '<div class="sd_flash '.$this->type.'">'.($this->translate ? lang('Flashdata.'.$this->message) : $this->message).'</div>';
    }

    /**
     * Returns a string containing the JSON corresponding to the Flash.
     *
     * @return string the JSON corresponding to this Flash
     */
    public function showJson(): string
    {
        return json_encode([
            'type' => $this->type,
            'message' => ($this->translate ? lang('Flashdata.'.$this->message) : $this->message),
        ]);
    }

    /**
     * This static method allows to add a Flash message on the same page or on the next one.
     *
     * @param string $message   the message to be displayed or the translation key corresponding to the message
     * @param string $type      The type of message, "info" by default. This must correspond to a CSS class.
     * @param bool   $translate If false (by default), the message will be displayed as is. If true, <code>lang(Flash.$message)</code> will be called, for example: <code>new Flash('helloWorld')</code> produces the message <code>lang('Flash.helloWorld')</code>.
     *@param $samePage if the Flash belongs to the same page instead of the next page
     */
    public static function add(string $message, string $type = 'info', bool $translate = false, bool $samePage = false): void
    {
        $flash = new Flash($message, $type, $translate);
        if ($samePage) {
            static::$samePageFlash[] = $flash;
        } else {
            $randomId = rand();
            service('session')->setFlashdata('sd_flash_'.$randomId, $flash);
        }
    }

    /**
     * This static method displays the Flash messages stored in the session before the reload and those added on the same page with <code>Flash::add()</code>.
     *
     * @return string every Flash messages HTML code separated by a new line
     */
    public static function display(): string
    {
        $flashes = service('session')->getFlashdata();
        $lines = [];
        foreach ($flashes as $flash) {
            $lines[] = $flash->show();
        }

        foreach (static::$samePageFlash as $flash) {
            $lines[] = $flash->show();
        }

        return implode(PHP_EOL, $lines);
    }
}
